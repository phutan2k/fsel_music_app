import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_music_app/blocs/music_blocs.dart';
import 'package:fsel_music_app/blocs/music_events.dart';
import 'package:fsel_music_app/blocs/music_states.dart';
import 'package:fsel_music_app/pages/detail_home_page/detail_home_page.dart';

import '../../widgets/reusable_text_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<MusicBloc>(context).add(InitMusics());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text('Music List'),
            centerTitle: true,
            backgroundColor: Colors.white,
          ),
          body: Column(
            children: [
              BlocBuilder<MusicBloc, MusicStates>(
                builder: (context, state) {
                  return Expanded(
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 10,
                        right: 10,
                        top: 20,
                      ),
                      child: ListView.builder(
                          itemCount: state.musicList.length,
                          itemBuilder: (_, index) {
                            return GestureDetector(
                              onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => DetailHomePage(
                                    musicModel: state.musicList[index],
                                  ),
                                ),
                              ),
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 100,
                                margin: EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(
                                  top: 20,
                                  bottom: 20,
                                  left: 20,
                                  right: 10,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    state.musicList[index].image.isEmpty
                                        ? Image.asset(
                                            'assets/images/bg_music.jpg',
                                            width: 60,
                                            height: 60,
                                            fit: BoxFit.cover,
                                          )
                                        : Image.asset(
                                            'assets/images/' +
                                                state.musicList[index].image,
                                            width: 60,
                                            height: 60,
                                            fit: BoxFit.cover,
                                          ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ReusableTextWidget(
                                            text: state.musicList[index].name,
                                            fontWeight: FontWeight.bold,
                                            size: 18,
                                          ),
                                          ReusableTextWidget(
                                            text: state.musicList[index].artist,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
