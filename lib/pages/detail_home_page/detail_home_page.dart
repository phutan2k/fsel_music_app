import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_music_app/blocs/music_blocs.dart';
import 'package:fsel_music_app/blocs/music_events.dart';
import 'package:fsel_music_app/blocs/music_states.dart';
import 'package:fsel_music_app/models/music_model.dart';

import '../../widgets/reusable_icon_widget.dart';
import '../../widgets/reusable_text_widget.dart';

class DetailHomePage extends StatefulWidget {
  const DetailHomePage({Key? key, required this.musicModel}) : super(key: key);
  final MusicModel musicModel;

  @override
  State<DetailHomePage> createState() => _DetailHomePageState();
}

class _DetailHomePageState extends State<DetailHomePage> {
  late AudioPlayer player;
  Duration? _duration;
  Duration? _position;

  late String name;
  late int id;
  late String artist;
  late String image;
  late String link;

  StreamSubscription? _durationSubscription;
  StreamSubscription? _positionSubscription;
  StreamSubscription? _playerCompleteSubscription;
  StreamSubscription? _playerStateChangeSubscription;
  PlayerState? _playerState;

  String get _durationText => _duration?.toString().split('.').first ?? '';

  String get _positionText => _position?.toString().split('.').first ?? '';

  @override
  void initState() {
    super.initState();
    // Create the audio player.
    player = AudioPlayer();

    name = widget.musicModel.name;
    id = widget.musicModel.id;
    artist = widget.musicModel.artist;
    image = widget.musicModel.image;
    link = widget.musicModel.link;

    player.setSource(AssetSource(link));
    BlocProvider.of<MusicBloc>(context).add(PlayMusic(false));

    player.getDuration().then(
          (value) => setState(() {
            _duration = value;
            print('Duration: $_duration');
          }),
        );
    player.getCurrentPosition().then(
          (value) => setState(() {
            _position = value;
            print('Position: $_position');
          }),
        );
    _playerState = player.state;

    _initStreams();
  }

  @override
  void dispose() {
    // Release all sources and dispose the player.
    player.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: Colors.white,
            child: BlocBuilder<MusicBloc, MusicStates>(
              builder: (context, state) {
                return MultiBlocListener(
                  listeners: [
                    BlocListener<MusicBloc, MusicStates>(
                      listenWhen: (previous, current) =>
                          previous.nextToNewSongStatus !=
                          current.nextToNewSongStatus,
                      listener: (context, state) {
                        if (state.nextToNewSongStatus == MusicStatus.success) {
                          name = state.model.name;
                          id = state.model.id;
                          artist = state.model.artist;
                          image = state.model.image;
                          link = state.model.link;
                        }
                      },
                    ),
                    BlocListener<MusicBloc, MusicStates>(
                      listenWhen: (previous, current) =>
                          previous.previousToOldStatus !=
                          current.previousToOldStatus,
                      listener: (context, state) {
                        if (state.previousToOldStatus == MusicStatus.success) {
                          name = state.model.name;
                          id = state.model.id;
                          artist = state.model.artist;
                          image = state.model.image;
                          link = state.model.link;
                        }
                      },
                    ),
                  ],
                  child: Column(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 65,
                        padding: EdgeInsets.only(right: 15),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: ReusableIconWidget(
                                  iconData: Icons.arrow_back_ios_new_outlined),
                            ),
                            ReusableTextWidget(
                              text: name,
                              size: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      image.isEmpty
                          ? Image.asset(
                              'assets/images/bg_music.jpg',
                              width: 250,
                              height: 250,
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              'assets/images/' + image,
                              width: 250,
                              height: 250,
                              fit: BoxFit.cover,
                            ),
                      SizedBox(
                        height: 5,
                      ),
                      ReusableTextWidget(text: artist),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                if (id < state.musicList.length && id >= 2) {
                                  print(
                                      'Length of list: ${state.musicList.length}');
                                  print('Old model: $id');
                                  MusicModel model = state.musicList[id - 2];
                                  BlocProvider.of<MusicBloc>(context)
                                      .add(PreviousToOldSong(model));
                                }
                              },
                              child: ReusableIconWidget(
                                iconData: Icons.skip_previous,
                                iconSize: 36,
                              ),
                            ),
                            SizedBox(width: 25),
                            InkWell(
                              onTap: () {
                                print('Tapped');
                                if (state.isPlayed == true) {
                                  print('Tapped 2');
                                  player.pause();
                                  BlocProvider.of<MusicBloc>(context)
                                      .add(PlayMusic(false));
                                } else {
                                  print('Tapped 3');
                                  player.dispose();
                                  player.play(AssetSource(link));
                                  BlocProvider.of<MusicBloc>(context)
                                      .add(PlayMusic(true));
                                }
                              },
                              child: state.isPlayed == true
                                  ? ReusableIconWidget(
                                      iconData: Icons.pause, iconSize: 36)
                                  : ReusableIconWidget(
                                      iconData: Icons.play_arrow, iconSize: 36),
                            ),
                            SizedBox(width: 25),
                            InkWell(
                              onTap: () {
                                if (id < state.musicList.length) {
                                  print(
                                      'Length of list: ${state.musicList.length}');
                                  print('Old model: $id');
                                  MusicModel model = state.musicList[id];
                                  BlocProvider.of<MusicBloc>(context)
                                      .add(NextToNewSong(model));
                                }
                              },
                              child: ReusableIconWidget(
                                iconData: Icons.skip_next,
                                iconSize: 36,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Slider(
                        value: (_position != null &&
                                _duration != null &&
                                _position!.inMilliseconds > 0 &&
                                _position!.inMilliseconds <
                                    _duration!.inMilliseconds)
                            ? _position!.inMilliseconds /
                                _duration!.inMilliseconds
                            : 0.0,
                        onChanged: (value) {
                          final duration = _duration;
                          print('Duration when tapped: $duration');

                          if (duration == null) {
                            return;
                          }
                          final position = value * duration.inMilliseconds;
                          print('Position when tapped: $position');

                          player.seek(Duration(milliseconds: position.round()));
                        },
                        activeColor: Colors.red,
                      ),
                      Text(
                        _position != null
                            ? '$_positionText / $_durationText'
                            : _duration != null
                                ? _durationText
                                : '',
                        style: const TextStyle(fontSize: 16.0),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      InkWell(
                          onTap: () {},
                          child: ReusableIconWidget(
                            iconData: Icons.speaker,
                            iconSize: 36,
                          ))
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void _initStreams() {
    _durationSubscription = player.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);
    });

    _positionSubscription = player.onPositionChanged.listen(
      (p) => setState(() => _position = p),
    );

    _playerCompleteSubscription = player.onPlayerComplete.listen((event) {
      setState(() {
        _playerState = PlayerState.stopped;
        _position = Duration.zero;
      });
    });

    _playerStateChangeSubscription =
        player.onPlayerStateChanged.listen((state) {
      setState(() {
        _playerState = state;
      });
    });
  }
}
