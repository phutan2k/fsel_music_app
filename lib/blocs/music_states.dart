import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../models/music_model.dart';

part 'music_states.g.dart';

enum MusicStatus { init, loading, success, fail }

@JsonSerializable()
@CopyWith()
class MusicStates extends Equatable {
  final List<MusicModel> musicList;
  final bool isPlayed;
  final MusicModel model;
  final MusicStatus nextToNewSongStatus;
  final MusicStatus previousToOldStatus;
  final double value;

  MusicStates({
    this.musicList = const [],
    this.isPlayed = false,
    required this.model,
    this.nextToNewSongStatus = MusicStatus.init,
    this.previousToOldStatus = MusicStatus.init,
    this.value = 0.0,
  });

  @override
  List<Object?> get props => [
        musicList,
        isPlayed,
        model,
        nextToNewSongStatus,
        previousToOldStatus,
        value,
      ];

  factory MusicStates.fromJson(Map<String, dynamic> json) =>
      _$MusicStatesFromJson(json);

  Map<String, dynamic> toJson() => _$MusicStatesToJson(this);
}
