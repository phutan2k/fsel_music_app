import 'dart:async';

import 'package:fsel_music_app/blocs/music_events.dart';
import 'package:fsel_music_app/blocs/music_states.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../models/music_model.dart';

class MusicBloc extends HydratedBloc<MusicEvents, MusicStates> {
  MusicBloc()
      : super(MusicStates(
            model:
                MusicModel(id: 0, link: '', artist: '', name: '', image: ''))) {
    on<PlayMusic>(_playMusic);
    on<InitMusics>(_onInitMusics);
    on<NextToNewSong>(_nextToNewSong);
    on<PreviousToOldSong>(_previousToOldSong);
    on<RewindMusic>(_rewindMusic);
  }

  @override
  MusicStates? fromJson(Map<String, dynamic> json) {
    return MusicStates.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(MusicStates state) {
    return state.toJson();
  }

  FutureOr<void> _playMusic(
    PlayMusic event,
    Emitter<MusicStates> emit,
  ) {
    print('Event: ${event.isPlayed}');
    emit(state.copyWith(isPlayed: event.isPlayed));
  }

  FutureOr<void> _onInitMusics(InitMusics event, Emitter<MusicStates> emit) {
    final List<MusicModel> musicList = [
      MusicModel(
        id: 1,
        name: 'Bình yên những phút giây',
        artist: 'Sơn Tùng M-TP',
        link: 'BinhYenNhungPhutGiay.mp3',
      ),
      MusicModel(
        id: 2,
        name: 'Chúng ta của hiện tại',
        artist: 'Sơn Tùng M-TP',
        link: 'ChungTaCuaHienTai.mp3',
      ),
      MusicModel(
        id: 3,
        name: 'Mùa đông nhớ anh',
        artist: 'Tô Ngọc Hà',
        link: 'MuaDongNhoAnh.mp3',
      ),
      MusicModel(
        id: 4,
        name: 'Hơn cả yêu',
        artist: 'Đức Phúc',
        link: 'HonCaYeu.mp3',
      ),
      MusicModel(
        id: 5,
        name: 'I do',
        artist: 'Đức Phúc',
        link: 'EmDongY.mp3',
      ),
    ];
    emit(state.copyWith(musicList: musicList));
  }

  FutureOr<void> _nextToNewSong(
    NextToNewSong event,
    Emitter<MusicStates> emit,
  ) {
    emit(state.copyWith(nextToNewSongStatus: MusicStatus.loading));
    print('New model: ${event.model.id}');
    emit(state.copyWith(
        model: event.model, nextToNewSongStatus: MusicStatus.success));
  }

  FutureOr<void> _previousToOldSong(
    PreviousToOldSong event,
    Emitter<MusicStates> emit,
  ) {
    emit(state.copyWith(previousToOldStatus: MusicStatus.loading));
    emit(state.copyWith(
        model: event.model, previousToOldStatus: MusicStatus.success));
  }

  FutureOr<void> _rewindMusic(
    RewindMusic event,
    Emitter<MusicStates> emit,
  ) {
    emit(state.copyWith(value: event.value));
  }
}
