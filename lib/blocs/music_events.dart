import 'package:equatable/equatable.dart';
import 'package:fsel_music_app/models/music_model.dart';

sealed class MusicEvents extends Equatable {
  MusicEvents();

  @override
  List<Object?> get props => [];
}

class PlayMusic extends MusicEvents {
  final bool isPlayed;

  PlayMusic(this.isPlayed);

  @override
  List<Object?> get props => [];
}

class InitMusics extends MusicEvents {
  InitMusics();

  @override
  List<Object?> get props => [];
}

class NextToNewSong extends MusicEvents {
  final MusicModel model;

  NextToNewSong(this.model);

  @override
  List<Object?> get props => [];
}

class PreviousToOldSong extends MusicEvents {
  final MusicModel model;

  PreviousToOldSong(this.model);

  @override
  List<Object?> get props => [];
}

class RewindMusic extends MusicEvents {
  final double value;

  RewindMusic(this.value);

  @override
  List<Object?> get props => [];
}
