// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music_states.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$MusicStatesCWProxy {
  MusicStates isPlayed(bool isPlayed);

  MusicStates model(MusicModel model);

  MusicStates musicList(List<MusicModel> musicList);

  MusicStates nextToNewSongStatus(MusicStatus nextToNewSongStatus);

  MusicStates previousToOldStatus(MusicStatus previousToOldStatus);

  MusicStates value(double value);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MusicStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MusicStates(...).copyWith(id: 12, name: "My name")
  /// ````
  MusicStates call({
    bool? isPlayed,
    MusicModel? model,
    List<MusicModel>? musicList,
    MusicStatus? nextToNewSongStatus,
    MusicStatus? previousToOldStatus,
    double? value,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfMusicStates.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfMusicStates.copyWith.fieldName(...)`
class _$MusicStatesCWProxyImpl implements _$MusicStatesCWProxy {
  final MusicStates _value;

  const _$MusicStatesCWProxyImpl(this._value);

  @override
  MusicStates isPlayed(bool isPlayed) => this(isPlayed: isPlayed);

  @override
  MusicStates model(MusicModel model) => this(model: model);

  @override
  MusicStates musicList(List<MusicModel> musicList) =>
      this(musicList: musicList);

  @override
  MusicStates nextToNewSongStatus(MusicStatus nextToNewSongStatus) =>
      this(nextToNewSongStatus: nextToNewSongStatus);

  @override
  MusicStates previousToOldStatus(MusicStatus previousToOldStatus) =>
      this(previousToOldStatus: previousToOldStatus);

  @override
  MusicStates value(double value) => this(value: value);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MusicStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MusicStates(...).copyWith(id: 12, name: "My name")
  /// ````
  MusicStates call({
    Object? isPlayed = const $CopyWithPlaceholder(),
    Object? model = const $CopyWithPlaceholder(),
    Object? musicList = const $CopyWithPlaceholder(),
    Object? nextToNewSongStatus = const $CopyWithPlaceholder(),
    Object? previousToOldStatus = const $CopyWithPlaceholder(),
    Object? value = const $CopyWithPlaceholder(),
  }) {
    return MusicStates(
      isPlayed: isPlayed == const $CopyWithPlaceholder() || isPlayed == null
          ? _value.isPlayed
          // ignore: cast_nullable_to_non_nullable
          : isPlayed as bool,
      model: model == const $CopyWithPlaceholder() || model == null
          ? _value.model
          // ignore: cast_nullable_to_non_nullable
          : model as MusicModel,
      musicList: musicList == const $CopyWithPlaceholder() || musicList == null
          ? _value.musicList
          // ignore: cast_nullable_to_non_nullable
          : musicList as List<MusicModel>,
      nextToNewSongStatus:
          nextToNewSongStatus == const $CopyWithPlaceholder() ||
                  nextToNewSongStatus == null
              ? _value.nextToNewSongStatus
              // ignore: cast_nullable_to_non_nullable
              : nextToNewSongStatus as MusicStatus,
      previousToOldStatus:
          previousToOldStatus == const $CopyWithPlaceholder() ||
                  previousToOldStatus == null
              ? _value.previousToOldStatus
              // ignore: cast_nullable_to_non_nullable
              : previousToOldStatus as MusicStatus,
      value: value == const $CopyWithPlaceholder() || value == null
          ? _value.value
          // ignore: cast_nullable_to_non_nullable
          : value as double,
    );
  }
}

extension $MusicStatesCopyWith on MusicStates {
  /// Returns a callable class that can be used as follows: `instanceOfMusicStates.copyWith(...)` or like so:`instanceOfMusicStates.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$MusicStatesCWProxy get copyWith => _$MusicStatesCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MusicStates _$MusicStatesFromJson(Map<String, dynamic> json) => MusicStates(
      musicList: (json['musicList'] as List<dynamic>?)
              ?.map((e) => MusicModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      isPlayed: json['isPlayed'] as bool? ?? false,
      model: MusicModel.fromJson(json['model'] as Map<String, dynamic>),
      nextToNewSongStatus: $enumDecodeNullable(
              _$MusicStatusEnumMap, json['nextToNewSongStatus']) ??
          MusicStatus.init,
      previousToOldStatus: $enumDecodeNullable(
              _$MusicStatusEnumMap, json['previousToOldStatus']) ??
          MusicStatus.init,
      value: (json['value'] as num?)?.toDouble() ?? 0.0,
    );

Map<String, dynamic> _$MusicStatesToJson(MusicStates instance) =>
    <String, dynamic>{
      'musicList': instance.musicList,
      'isPlayed': instance.isPlayed,
      'model': instance.model,
      'nextToNewSongStatus':
          _$MusicStatusEnumMap[instance.nextToNewSongStatus]!,
      'previousToOldStatus':
          _$MusicStatusEnumMap[instance.previousToOldStatus]!,
      'value': instance.value,
    };

const _$MusicStatusEnumMap = {
  MusicStatus.init: 'init',
  MusicStatus.loading: 'loading',
  MusicStatus.success: 'success',
  MusicStatus.fail: 'fail',
};
