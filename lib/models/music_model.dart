import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'music_model.g.dart';

@JsonSerializable()
@CopyWith()
class MusicModel {
  final int id;
  final String name;
  final String artist;
  final String image;
  final String link;

  MusicModel({
    required this.id,
    required this.name,
    required this.artist,
    this.image = '',
    required this.link,
  });

  factory MusicModel.fromJson(Map<String, dynamic> json) =>
      _$MusicModelFromJson(json);

  Map<String, dynamic> toJson() => _$MusicModelToJson(this);
}
