// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'music_model.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$MusicModelCWProxy {
  MusicModel artist(String artist);

  MusicModel id(int id);

  MusicModel image(String image);

  MusicModel link(String link);

  MusicModel name(String name);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MusicModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MusicModel(...).copyWith(id: 12, name: "My name")
  /// ````
  MusicModel call({
    String? artist,
    int? id,
    String? image,
    String? link,
    String? name,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfMusicModel.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfMusicModel.copyWith.fieldName(...)`
class _$MusicModelCWProxyImpl implements _$MusicModelCWProxy {
  final MusicModel _value;

  const _$MusicModelCWProxyImpl(this._value);

  @override
  MusicModel artist(String artist) => this(artist: artist);

  @override
  MusicModel id(int id) => this(id: id);

  @override
  MusicModel image(String image) => this(image: image);

  @override
  MusicModel link(String link) => this(link: link);

  @override
  MusicModel name(String name) => this(name: name);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `MusicModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// MusicModel(...).copyWith(id: 12, name: "My name")
  /// ````
  MusicModel call({
    Object? artist = const $CopyWithPlaceholder(),
    Object? id = const $CopyWithPlaceholder(),
    Object? image = const $CopyWithPlaceholder(),
    Object? link = const $CopyWithPlaceholder(),
    Object? name = const $CopyWithPlaceholder(),
  }) {
    return MusicModel(
      artist: artist == const $CopyWithPlaceholder() || artist == null
          ? _value.artist
          // ignore: cast_nullable_to_non_nullable
          : artist as String,
      id: id == const $CopyWithPlaceholder() || id == null
          ? _value.id
          // ignore: cast_nullable_to_non_nullable
          : id as int,
      image: image == const $CopyWithPlaceholder() || image == null
          ? _value.image
          // ignore: cast_nullable_to_non_nullable
          : image as String,
      link: link == const $CopyWithPlaceholder() || link == null
          ? _value.link
          // ignore: cast_nullable_to_non_nullable
          : link as String,
      name: name == const $CopyWithPlaceholder() || name == null
          ? _value.name
          // ignore: cast_nullable_to_non_nullable
          : name as String,
    );
  }
}

extension $MusicModelCopyWith on MusicModel {
  /// Returns a callable class that can be used as follows: `instanceOfMusicModel.copyWith(...)` or like so:`instanceOfMusicModel.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$MusicModelCWProxy get copyWith => _$MusicModelCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MusicModel _$MusicModelFromJson(Map<String, dynamic> json) => MusicModel(
      id: json['id'] as int,
      name: json['name'] as String,
      artist: json['artist'] as String,
      image: json['image'] as String? ?? '',
      link: json['link'] as String,
    );

Map<String, dynamic> _$MusicModelToJson(MusicModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'artist': instance.artist,
      'image': instance.image,
      'link': instance.link,
    };
